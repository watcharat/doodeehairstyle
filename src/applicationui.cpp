#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/platform/geo/GeoLocation.hpp>

#include "ProfileEditor.hpp"
#include "RegistrationHandler.hpp"
#include <bb/system/SystemUiResult>

#include <bps/soundplayer.h>

using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
		QObject(app) {
	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);
	if (!QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()),
			this, SLOT(onSystemLanguageChanged()))) {
		// This is an abnormal situation! Something went wrong!
		// Add own code to recover here
		qWarning() << "Recovering from a failed connect()";
	}
	// initial load
	onSystemLanguageChanged();

	qmlRegisterType<bb::cascades::advertisement::Banner>(
			"bb.cascades.advertisement", 1, 0, "Banner");
	qmlRegisterType<Camera>("bb.cascades.multimedia", 1, 0, "Camera");
	qmlRegisterType<pickers::FilePicker>("bb.cascades.pickers", 1, 0,
			"FilePicker");
	qmlRegisterUncreatableType<pickers::FilePickerMode>("bb.cascades.pickers",
			1, 0, "FilePickerMode", "");
	qmlRegisterUncreatableType<pickers::FilePickerSortFlag>(
			"bb.cascades.pickers", 1, 0, "FilePickerSortFlag", "");
	qmlRegisterUncreatableType<pickers::FilePickerSortOrder>(
			"bb.cascades.pickers", 1, 0, "FilePickerSortOrder", "");
	qmlRegisterUncreatableType<pickers::FileType>("bb.cascades.pickers", 1, 0,
			"FileType", "");
	qmlRegisterUncreatableType<pickers::FilePickerViewMode>(
			"bb.cascades.pickers", 1, 0, "FilePickerViewMode", "");

	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
	qml->setContextProperty("monster", this);

	//bbm
	const QString uuid(QLatin1String("d22e1cc0-3b36-434b-89a9-a0ff0a796225"));
	RegistrationHandler *registrationHandler = new RegistrationHandler(uuid,
			app);
	qml->setContextProperty("_registrationHandler", registrationHandler);
	profile = new Profile(registrationHandler->context(), app);
	qml->setContextProperty("_profile", profile);
	inviteToDownload = new InviteToDownload(registrationHandler->context(),
			app);
	QObject::connect(registrationHandler, SIGNAL(registered()),
			inviteToDownload, SLOT(show()));
	qml->setContextProperty("_inviteToDownload", inviteToDownload);
	connect(inviteToDownload, SIGNAL(inviteSuccess(bool)), this,
			SLOT(onInviteSuccess(bool)));

	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();

	// Set created root object as the application scene
	app->setScene(root);

	// Initialize
	// Application Folder
	appFolder = QDir::homePath();
	appFolder.chop(4);

	// Work Folder and Temp
	workFolder = "camera/ddhs";
	workFolderTmp = "camera/ddhs_tmp";

	QDir qdir(appFolder + "shared/" + workFolder);
	if (!qdir.exists()) {
		qdir.mkpath(appFolder + "shared/" + workFolder);
	}
	QDir qdirTmp(appFolder + "shared/" + workFolderTmp);
	if (!qdirTmp.exists()) {
		qdirTmp.mkpath(appFolder + "shared/" + workFolderTmp);
	}

	QObject *imgViewBG = root->findChild<QObject*>("objImageBG");
	if (imgViewBG) {
		objImageBG = imgViewBG;
	}
	QObject *imgView = root->findChild<QObject*>("objImageDesignName");
	if (imgView) {
		objImage = imgView;
	}

	//camera
	camera = root->findChild<Camera*>("myCamera");
	QObject::connect(camera, SIGNAL(shutterFired()), this,
			SLOT(onShutterFired()));

	//must check camera's state before open it
	// Retrieve a list of accessible camera units.
	QList<CameraUnit::Type> list = camera->supportedCameras();

	if (list.isEmpty() || list.contains(CameraUnit::None)) {
		// Report: "No cameras are accessible."
		//show error and quit app
		showError(
				"The camera is in use. Close any application using it and try again.");
		app->quit();
	} else {
		if (list.contains(CameraUnit::Rear)) {
			// The default camera unit is rear
			// so no parameter is necessary.
			camera->open(CameraUnit::Rear);
			cameraRear = true;

			CameraSettings *setting = new CameraSettings();
			camera->getSettings(setting);
			setting->setFocusMode(CameraFocusMode::ContinuousAuto);
			camera->applySettings(setting);
			setting = 0;
		} else if (list.contains(CameraUnit::Front)) {
			// Open the front camera unit.
			camera->open(CameraUnit::Front);
			cameraRear = false;

			CameraSettings *setting = new CameraSettings();
			camera->getSettings(setting);
			setting->setFocusMode(CameraFocusMode::ContinuousAuto);
			camera->applySettings(setting);
			setting = 0;
		} else {
			// Report unknown error.
			//show error and quit app
			showError(
					"The camera is in use. Close any application using it and try again.");
			//				app->quit();
			Application::instance()->exit(0);
		}
	}
}

void ApplicationUI::onSystemLanguageChanged() {
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("DooDeeHairStyle_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}

ApplicationUI::~ApplicationUI() {
	QFile qfile(appFolder + "shared/" + workFolderTmp + "/obj_tmp.png");
	if (qfile.exists()) {
		qfile.remove();
		qfile.flush();
	}

	QDir qdir(appFolder + "shared/" + workFolderTmp);
	if (qdir.exists()) {
		qdir.cleanPath(appFolder + "shared/" + workFolderTmp);
		qdir.rmdir(appFolder + "shared/" + workFolderTmp);
	}

}

void ApplicationUI::showError(QString wordBody) {
	popupError = new SystemDialog("OK");
	QString wordHead("Error!");
	popupError->setTitle(wordHead);
	popupError->setBody(wordBody);
	popupError->show();
}

long ApplicationUI::getTimeMs() {
	QDateTime time = QDateTime::currentDateTime();
	return time.toMSecsSinceEpoch();
}

void ApplicationUI::genObjectSize(const QString &name, const int no) {
	QImageReader reader_;
	QString prePath = appFolder + "app/native/assets/images/" + name + "/"
			+ QString::number(no) + ".png";
	reader_.setFileName(prePath);
	QImage obj_reader = reader_.read();
	QSize imageSizeWH = obj_reader.size();

	obj_w = imageSizeWH.width();
	obj_h = imageSizeWH.height();

}

int ApplicationUI::getObjW() {
	return obj_w;
}

int ApplicationUI::getObjH() {
	return obj_h;
}

void ApplicationUI::onInviteSuccess(bool success) {
	if (success) {
		qDebug() << "onInviteSuccess(): true";
	} else {
		qDebug() << "onInviteSuccess(): false";
	}
}

void ApplicationUI::switchCamera() {
	cameraRear = !cameraRear;
	camera->close();
	restartCamera();
}

void ApplicationUI::restartCamera() {
	qDebug() << "restartCamera()";
	setCameraMode(true);
	camera->setVisible(true);
	selectedFilePath = "";
	imageView->setVisible(false);
	if (cameraRear) {
		camera->open(CameraUnit::Rear);
	} else {
		camera->open(CameraUnit::Front);
	}
}

void ApplicationUI::closeCamera() {
	qDebug() << "Close Camera";
	setCameraMode(false);
	camera->close();
	camera->setVisible(false);
}

void ApplicationUI::setCameraMode(bool mode) {
	cameraMode = mode;
	emit cameraModeChanged(cameraMode);
}

bool ApplicationUI::isCameraMode() {
	return cameraMode;
}

void ApplicationUI::deleteSelectedFilePath() {
	qDebug() << "deleteSelectedFilePath: " + selectedFilePath;
	if (selectedFilePath != "") {
		QFile *file = new QFile(selectedFilePath);
		if (file->exists()) {
			if (file->remove()) {
				qDebug() << "REMOVE SUCCESS";
				selectedFilePath = "";
			} else {
				qDebug() << "REMOVE FAIL";
			}
		}
	}
}

void ApplicationUI::setSelectedFilePath(const QString &path) {
	qDebug() << "setSelectedFilePath():" + path;
	deleteSelectedFilePath();
	lastedPath = path;
//	selectedFilePath = path;
	//check wheter size of image
	reader.setFileName(path);
	icon = reader.read();
	iconSize = icon.size();
	if (iconSize.width() >= iconSize.height()) {
		//scale to height
		icon = icon.scaledToHeight(1024, Qt::SmoothTransformation);
		if (icon.size().width() > 768) {
			//crop width
			qDebug() << "crop width";
			icon = icon.copy((icon.size().width() - 768) / 2, 0, 768, 1024);
		}
	} else if (icon.height() > iconSize.width()) {
		//scale to width
		icon = icon.scaledToWidth(768, Qt::SmoothTransformation);
		if (icon.size().height() > 1024) {
			//crop height
			qDebug() << "crop height";
			icon = icon.copy(0, (icon.size().height() - 1024) / 2, 768, 1024);
		}
	}/*else{
	 //width = heigth
	 //		icon = icon.scaled(768,1024,Qt::KeepAspectRatio, Qt::SmoothTransformation);
	 icon = icon.scaledToHeight(1024, Qt::SmoothTransformation);
	 }*/

	qDebug() << "icon width: " + QString::number(icon.size().width());
	qDebug() << "icon hight: " + QString::number(icon.size().height());

	qDebug() << "path:" + path;
	imagefileName = path.mid(path.lastIndexOf('/') + 1, path.lastIndexOf('.'));
	imagefileName.chop(4);
	imagefileName = imagefileName + "_"
			+ QString::number(QDateTime::currentMSecsSinceEpoch()) + ".jpg";
	qDebug() << "imagefileName:" + imagefileName;

	QDir dir;
	qDebug() << "BEFORE QDir::currentPath() = " + QDir::currentPath();
	qDebug() << "BEFORE dir.path() = " + dir.path();
	dir.cd("tmp/");

	qDebug() << "AFTER QDir::currentPath() = " + QDir::currentPath();
	qDebug() << "AFTER dir.path() = " + dir.path();

	QString pathMain = /*"file://" +*/QDir::currentPath() + "/" + dir.path()
			+ "/";
	selectedFilePath = pathMain + imagefileName;
//	QString oldPath = assetFolder + imagefileName;
//	qDebug() << "OLD selectedFilePath: " + oldPath;
	qDebug() << "NEW selectedFilePath: " + selectedFilePath;

//	showError("selectedFilePath : "+selectedFilePath);
	if (icon.save(selectedFilePath, "JPG")) {
		imageView->setImageSource(QUrl(selectedFilePath));
		imageView->setVisible(true);
		qDebug() << "SAVE SUCCESS!";
	} else {
		qDebug() << "SAVE FAILED!";
		//popup
		selectedFilePath = "";
		showError(
				"Sorry, there is a problem about select image. Please try again.");
		restartCamera();

	}

//	imageView->setImage(icon);
}

QString ApplicationUI::getSelectedFilePath() {
	return selectedFilePath;
}

void ApplicationUI::onShutterFired() {
	soundplayer_play_sound("event_camera_shutter");
}

void ApplicationUI::showPhotoInCard(const QString fileName) {
// Create InvokeManager and InvokeRequest objects to able to invoke a card with a viewer that will show the
// latest bomber photo.
	bb::system::InvokeManager manager;
	bb::system::InvokeRequest request;

// Setup what to show and in what target.
	request.setUri(QUrl::fromLocalFile(fileName));
	request.setTarget("sys.pictures.card.previewer");
	request.setAction("bb.action.VIEW");
	InvokeTargetReply *targetReply = manager.invoke(request);
//setting the parent to "this" will make the manager live on after this function is destroyed
	manager.setParent(this);

	if (targetReply == NULL) {
		qDebug() << "InvokeTargetReply is NULL: targetReply = " << targetReply;
	} else {
		targetReply->setParent(this);
	}
}

void ApplicationUI::saveImage() {
	qDebug() << "saveImage()";

	onShutterFired();
	//write image to camera folder

	//copy to camera foldeimagefileName
	reader.setFileName(selectedFilePath);
	icon = reader.read();
	QString pathSave = QDir::currentPath() + "/shared/camera/" + imagefileName;
	qDebug() << "pathSave: " + pathSave;
	if (icon.save(pathSave, "JPG")) {
		deleteSelectedFilePath();
		manipulatePhoto(pathSave);
//		deleteSelectedFilePath();
		setSelectedFilePath(lastedPath); //create new one
	} else {
		showError("Can't save image. Please try again.");
	}
}

void ApplicationUI::manipulatePhoto(const QString &fileName) {
	qDebug()
			<< "manipulatePhoto() designIndex = "
					+ QString::number(designIndex);

	QImageReader reader;

// Set image name from the given file name.
	reader.setFileName(fileName);
	QImage image = reader.read();
	image = image.scaledToWidth(768, Qt::SmoothTransformation);
	QSize imageSize = image.size();
	qDebug() << "-------> image width = " + QString::number(imageSize.width());
	qDebug()
			<< "-------> image height = " + QString::number(imageSize.height());

	QPainter painter;
	if (cameraMode && !cameraRear) {
		qDebug() << "CAPTURE FRONT CAMERA";
		QColor color;
		QImage newImage(imageSize.width(), imageSize.height(),
				QImage::Format_ARGB32_Premultiplied);
		//Front Camera >> need to reflect image
		for (int y = 0; y < imageSize.height(); y++) {
			for (int x = 0; x < imageSize.width(); x++) {
				color = QColor(image.pixel(x, y));
				newImage.setPixel(imageSize.width() - x - 1, y, color.rgb());
			}
		}
//		QPainter painter(&newImage);
//		if (designIndex == 0) {
//			paintImage1(painter, imageSize);
//		} else if (designIndex == 1) {
//			paintImage2(painter, imageSize);
//		} else if (designIndex == 2) {
//			paintImage3(painter, imageSize);
//		} else if (designIndex == 3) {
//			paintImage4(painter, imageSize);
//		} else if (designIndex == 4) {
//			paintImage5(painter, imageSize);
//		} else if (designIndex == 5) {
//			paintImage6(painter, imageSize);
//		} else if (designIndex == 6) {
//			paintImage7(painter, imageSize);
//		} else if (designIndex == 7) {
//			paintImage8(painter, imageSize);
//		} else if (designIndex == 8) {
//			paintImage9(painter, imageSize);
//		} else if (designIndex == 9) {
//			paintImage10(painter, imageSize);
//		}
		newImage.save(fileName, "JPG");
		showPhotoInCard(fileName);
	} else {
		qDebug() << "CAPTURE REAR CAMERA";
//		QPainter painter(&image);
//		if (designIndex == 0) {
//			paintImage1(painter, imageSize);
//		} else if (designIndex == 1) {
//			paintImage2(painter, imageSize);
//		} else if (designIndex == 2) {
//			paintImage3(painter, imageSize);
//		} else if (designIndex == 3) {
//			paintImage4(painter, imageSize);
//		} else if (designIndex == 4) {
//			paintImage5(painter, imageSize);
//		} else if (designIndex == 5) {
//			paintImage6(painter, imageSize);
//		} else if (designIndex == 6) {
//			paintImage7(painter, imageSize);
//		} else if (designIndex == 7) {
//			paintImage8(painter, imageSize);
//		} else if (designIndex == 8) {
//			paintImage9(painter, imageSize);
//		} else if (designIndex == 9) {
//			paintImage10(painter, imageSize);
//		}
		image.save(fileName, "JPG");
		showPhotoInCard(fileName);
	}

}
