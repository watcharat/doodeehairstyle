#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>

//#include "DataBase.h"

#include <QObject>
#include <QtGui/qpainter.h>

#include <QtLocationSubset/QGeoPositionInfo>
#include <QtLocationSubset/QGeoPositionInfoSource>
#include <bb/cascades/multimedia/Camera>

#include <bb/cascades/multimedia/CameraUnit>

#include <bb/cascades/pickers/FilePicker>
#include <bb/cascades/pickers/FilePickerMode>
#include <bb/cascades/pickers/FilePickerSortFlag>
#include <bb/cascades/pickers/FilePickerSortOrder>
#include <bb/cascades/pickers/FilePickerViewMode>
#include <bb/cascades/pickers/FileType>

#include "InviteToDownload.hpp"
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeTargetReply>
#include <bb/cascades/advertisement/Banner>

#include <bb/system/SystemDialog>
#include "Profile.hpp"

namespace bb {
namespace cascades {
class Application;
class LocaleHandler;
}
}

class QTranslator;

using namespace bb::cascades;
using namespace bb::system;
using namespace QtMobilitySubset;
using namespace bb::cascades::multimedia;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI: public QObject {
Q_OBJECT
public:
	ApplicationUI(bb::cascades::Application *app);
//    virtual ~ApplicationUI() { }
	~ApplicationUI();

	Q_INVOKABLE
	void genObjectSize(const QString &name, const int no);

	Q_INVOKABLE
	int getObjW();

	Q_INVOKABLE
	int getObjH();

	Q_INVOKABLE
	void saveImage();

	Q_INVOKABLE
	void restartCamera();

	Q_INVOKABLE
	void closeCamera();

	Q_INVOKABLE
	bool loadComplete();

	Q_INVOKABLE
	void setSelectedFilePath(const QString &path);
	void deleteSelectedFilePath();

	Q_INVOKABLE
	QString getSelectedFilePath();

	Q_INVOKABLE
	void manipulatePhoto(const QString &fileName);

	Q_INVOKABLE
	void showPhotoInCard(const QString fileName);

	void showError(QString wordBody);

	Q_INVOKABLE
	void switchCamera();

	void setCameraMode(bool mode);
	bool isCameraMode();

signals:
	void cameraModeChanged(bool);

private:
	QTranslator* m_pTranslator;
	bb::cascades::LocaleHandler* m_pLocaleHandler;

	long getTimeMs();

	QString appFolder;
	QString workFolder;
	QString workFolderTmp;

	QObject *objImageBG;
	QObject *objImage;

	int obj_w;
	int obj_h;

	//----------------------------
	bool cameraRear;
	Profile *profile;
	InviteToDownload *inviteToDownload;
	SystemDialog *popupError;
	Camera *camera;
	bool cameraMode;

	//paint
	float topY;
	float imageWidth;
	float imageHeight;

	//write image to fileSystem
	QString imagefileName;
	QString selectedFilePath;
	QString lastedPath;
	QString iconfileName;
	QImageReader reader;
	QString character;
	QImage icon;
	QSize iconSize;

private slots:
	void onSystemLanguageChanged();
	void onInviteSuccess(bool success);
};

#endif /* ApplicationUI_HPP_ */
