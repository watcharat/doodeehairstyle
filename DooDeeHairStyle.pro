APP_NAME = DooDeeHairStyle

CONFIG += qt warn_on cascades10
QT += network
LIBS += -lQtLocationSubset -lbbdata -lscreen -lcrypto -lcurl -lpackageinfo -lbbcascadesadvertisement -lbb -lbbplatformbbm

LIBS += -lbbcascadesmultimedia 
LIBS += -lbbsystem 
LIBS += -lbbcascadespickers
LIBS += -lbbdevice
LIBS += -lcamapi

include(config.pri)
