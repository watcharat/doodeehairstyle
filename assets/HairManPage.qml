import bb.cascades 1.0

Page {
    
    function genVisibleList(activeList) {
        defaultList.visible = false;
        styleAList.visible = false;
        
        if (activeList == "Default") {
            defaultList.visible = true;
        }
        if (activeList == "StyleA") {
            styleAList.visible = true;
        }
    
    }
    
    id: pgDetail
    actions: [
        ActionItem {
            title: qsTr("Default")
            onTriggered: {
                pgDetail.genVisibleList("Default");
            }
        },
        ActionItem {
            title: qsTr("StyleA")
            onTriggered: {
                pgDetail.genVisibleList("StyleA");
            }
        }
    ]
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            onTriggered: {
                navigationPane.pop()
            }
        }
    }
    
    Container {
        ListView {
            id: defaultList
            objectName: "defaultList"
            visible: true
            layout: GridListLayout {
                columnCount: 2
                headerMode: ListHeaderMode.Standard
                cellAspectRatio: 1.4
                spacingAfterHeader: 40
                verticalCellSpacing: 0
            
            }
            dataModel: XmlDataModel {
                source: "models/man_default.xml"
            }
            listItemComponents: [
                ListItemComponent {
                    type: "header"
                    Header {
                        title: {
                            if (ListItemData.title) {
                                ListItemData.title
                            } else {
                                ListItemData
                            }
                        }
                    }
                },
                ListItemComponent {
                    type: "item"
                    ObjectItem {
                    }
                }
            ]
            onTriggered: {
                if (indexPath.length > 1) {
                    var chosenItem = dataModel.data(indexPath);
                    var chosenPaht = "asset:///images/" + chosenItem.Model + "/" + chosenItem.ID + ".png";
                                        
                    imageBG.imageSource = chosenPaht;
                    monster.genObjectSize(chosenItem.Model, chosenItem.ID);
                    imageBG.preferredWidth = monster.getObjW(); // obj_w;
                    imageBG.preferredHeight = monster.getObjH(); // .obj_h;
                    navigationPane.pop()
                }
            }
        }
        
        ListView {
            id: styleAList
            objectName: "styleAList"
            visible: false
            layout: GridListLayout {
                columnCount: 2
                headerMode: ListHeaderMode.Standard
                cellAspectRatio: 1.4
                spacingAfterHeader: 40
                verticalCellSpacing: 0
            
            }
            dataModel: XmlDataModel {
                source: "models/hair.xml"
            }
            listItemComponents: [
                ListItemComponent {
                    type: "header"
                    Header {
                        title: {
                            if (ListItemData.title) {
                                ListItemData.title
                            } else {
                                ListItemData
                            }
                        }
                    }
                },
                ListItemComponent {
                    type: "item"
                    ObjectItem {
                    }
                }
            ]
            onTriggered: {
                if (indexPath.length > 1) {
                    var chosenItem = dataModel.data(indexPath);
                    var chosenPaht = "asset:///images/" + chosenItem.Model + "/" + chosenItem.ID + ".png";
                    
                    imageModify.imageSource = chosenPaht;
                    monster.genObjectSize(chosenItem.Model, chosenItem.ID);
                    imageModify.preferredWidth = monster.getObjW(); // obj_w;
                    imageModify.preferredHeight = monster.getObjH(); // .obj_h;
                    navigationPane.pop()
                }
            }
        }
        
        
    }
    
    
}
