import bb.cascades 1.0



TabbedPane {
    id: tabPaneMaster
//    showTabsOnActionBar: false
    showTabsOnActionBar: true
    Tab { //First tab
        id: firstTab
        imageSource: "images/Icon_Photo.png"
        // Localized text with the dynamic translation and locale updates support
        title: qsTr("Take Photo") + Retranslate.onLocaleOrLanguageChanged
        Page {
            Container {
                Label {
                    text: qsTr("Take Photo") + Retranslate.onLocaleOrLanguageChanged
                }
            }
        }
    } //End of first tab
    Tab { //Second tab
        imageSource: "images/Icon_Gallery.png"
        title: qsTr("Gallery") + Retranslate.onLocaleOrLanguageChanged
        Page {
            Container {
                Label {
                    text: qsTr("Gallery") + Retranslate.onLocaleOrLanguageChanged
                }
            }
        }
    } //End of second tab
    Tab { //Second tab
        imageSource: "images/Icon_Front.png"
        title: qsTr("New Hair Style") + Retranslate.onLocaleOrLanguageChanged
        
        NavigationPane {
            id: navigationPane
            
	        Page {
	            Container {
	                layout: AbsoluteLayout {
	                }
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Fill   
                    
	                // Image BG Area
	                ImageView {
	                    id: imageBG
	                    objectName: "objImageDesignName"
	                    //                objectName: "objImageBG"
	                    imageSource: "asset:///images/0.png" // "asset:///images/Fame_work_01.png"
	                    verticalAlignment: VerticalAlignment.Fill
	                    horizontalAlignment: HorizontalAlignment.Fill
	                }
	                
	                Container {
	                    layout: AbsoluteLayout {
	                    }
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill    
                        property int p_x
                        property int p_y
	                    ImageView {
	                        id: imageModify
                            preferredWidth: 500
                            preferredHeight: 500     
	                        imageSource: "asset:///images/2.png"
                            layoutProperties: AbsoluteLayoutProperties {
                                positionX: 0 // 100
                                positionY: 0 // 100    
                            }   
	                    }
                        onTouch: {
                            imageModify.translationX = event.localX - (imageModify.preferredWidth / 2);
                            imageModify.translationY = event.localY - (imageModify.preferredHeight / 2);
                            p_x = imageModify.translationX
                            p_y = imageModify.translationY  
                        }
	                }
	                
//	                Container {
//	                    layout: DockLayout {
//	                    }
//	                    Label {
//	                        text: qsTr("New Hair Style") + Retranslate.onLocaleOrLanguageChanged
//	                    }
//	                }
	                
	                
	            }
	            
	            
	            
	            actions: [
	                ActionItem {
	                    title: "Man"
	                    imageSource: "asset:///images/Icon_Gent.png"
	                    ActionBar.placement: ActionBarPlacement.OnBar
	                    onTriggered: {
	                        var page = getSecondPage();
	                        navigationPane.push(page);
	                    }
	                },
	                ActionItem {
	                    title: "Woman"
	                    imageSource: "asset:///images/Icon_Lady.png"
	                    ActionBar.placement: ActionBarPlacement.OnBar
	                    onTriggered: {
	                    }
	                }
	            ]
	            
	        }
        
        
        }
        
    } 
    
    property Page secondPage
    function getSecondPage() {
        if (! secondPage) {
            secondPage = hmpId.createObject();
        }
        return secondPage;
    }
    attachedObjects: [
        ComponentDefinition {
            id: hmpId
            source: "HairManPage.qml"
        }
    ]
    
    Menu.definition: MenuDefinition {
        settingsAction: SettingsActionItem {
            onTriggered: {
                var page_settings = getPage_Settings();
                navigationPane.push(page_settings);
            }
            property Page pageSettings
            function getPage_Settings() {
                if (! pageSettings) {
                    pageSettings = settingsPageId.createObject();
                }
                return pageSettings;
            }
        }
        helpAction: HelpActionItem {
            onTriggered: {
                var page_help = getPage_Help();
                navigationPane.push(page_help);
            }
            property Page pageHelp
            function getPage_Help() {
                if (! pageHelp) {
                    pageHelp = helpPageId.createObject();
                }
                return pageHelp;
            }
        }
        actions: [
            ActionItem {
                title: qsTr("Share")
                imageSource: "asset:///images/share_pressed.png"
                onTriggered: {
                    picker.open()
                }
            }
        ]
    }
}
